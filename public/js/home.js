$(document).ready(function(){

    listarPQR(1);

});

function listarPQR(page) {
    axios.get(`/api/pqr-web?page=${page}`).then(res => {
        $('#tbPQR tr').remove();
        $('.pagination li').remove();
        if(res.data.data.length > 0){
            for(pqr of res.data.data){
                $('#tbPQR').append(`<tr>
                                        <td>${pqr.nombre}</td>
                                        <td>${pqr.tipo_descripcion}</td>
                                        <td>${pqr.pqr_asunto}</td>
                                        <td>${moment(pqr.created_at).format('DD/MM/YYYY')}</td>
                                        <td>${((moment().isBefore(moment(pqr.fecha_limite).format('YYYY-MM-DD'))) ? moment(pqr.fecha_limite).format('DD/MM/YYYY') : `<span class="badge badge-danger">${moment(pqr.fecha_limite).format('DD/MM/YYYY')}</span>`)}</td>
                                        <td>${pqr.estado_descripcion} ${((pqr.estado_id !== 3) ? `<i class="fas fa-retweet" title="Cambiar Estado" onclick="cambiarEstado(${pqr.pqr_id})"></i>` : '')}</td>
                                        <td>
                                        ${((pqr.estado_id !== 3) ? `<button type="button" class="btn btn-primary" onclick="formEdit(${pqr.pqr_id})"><i class="fas fa-pen"></i></button>
                                            <button type="button" class="btn btn-danger" onclick="eliminar(${pqr.pqr_id})"><i class="fas fa-trash"></i></button>` : '')}
                                        </td>
                                    </tr>`);
            }

            for(link of res.data.links){
                $('.pagination').append(`<li class="page-item ${((link.active) ? 'active' : '')}"><a class="page-link" ${((link.url) ? `onclick="listarPQR(${parseInt(link.url.slice(-1))})` : '' )}">${link.label}</a></li>`);
            }
        }else{
            $('#tbPQR').append(`<tr><td colspan="7" class="text-center">No hay PQR registradas</td></tr>`);
        }
    });
}

function RegistrarPQR(){
    let form = $('#frmRegistro')[0];
    let url = form.getAttribute('action');
    let datos = new FormData(form);
    $('small').html('');

    Swal.showLoading()
    axios.post(url, datos).then(res => {
        Swal.fire({
            icon: 'success',
            title: 'Registro de PQR',
            text: res.data.Msj,
        });
        listarPQR(1);
        form.reset();
        $('#mdlRegisto').modal('hide');
    }, (err) => {
        let er = err.response.data.errors;
        Object.keys(er).forEach(function(key) {
            $(`#${key}-error`).text(er[key].toString().replace(/,/g, ' '))
        });
        Swal.close();
    });
}

function formEdit(id){
    axios.get(`/pqr/${id}/edit`).then(res => {
        let pqr = res.data;
        $('#hdPqr').val(pqr.pqr_id);
        $('#cbTipoEdit').val(pqr.tipo_id);
        $('#txtAsuntoEdit').val(pqr.pqr_asunto);
        $('#mdlEditar').modal();
    });
}

function EditarPQR(){
    let form = $('#frmEditar')[0];
    let accion = form.getAttribute('action');
    let url = `${accion}/${$('#hdPqr').val()}`;
    let datos = new FormData(form);
    $('small').html('');

    Swal.showLoading()
    axios.post(url, datos).then(res => {
        Swal.fire({
            icon: 'success',
            title: 'Actualización de PQR',
            text: res.data.Msj,
        });
        listarPQR(1);
        form.reset();
        $('#mdlEditar').modal('hide');
    }, (err) => {
        let er = err.response.data.errors;
        Object.keys(er).forEach(function(key) {
            $(`#${key}-error`).text(er[key].toString().replace(/,/g, ' '))
        });
        Swal.close();
    });
}

function eliminar(id){
    Swal.fire({
        title: '¿Esta Seguro?',
        text: "¿Desea eliminar la PQR?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Eliminar'
      }).then((result) => {
        if (result.isConfirmed) {
          axios.delete(`/pqr/${id}`).then(res => {
            Swal.fire(
                'Eliminar PQR',
                res.data.Msj,
                'success'
              );
            listarPQR(1);
          }, err => {
            Swal.fire(
                'Eliminar PQR',
                'Error al eliminar la PQR.',
                'error'
              );
          });
        }
      });
}

function cambiarEstado(id) {
    Swal.fire({
        title: '¿Esta Seguro?',
        text: "¿Desea cambiar el estado? Una vez cambiado no se puede regresar al estado anterior.",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar'
        }).then((result) => {
        if (result.isConfirmed) {
            axios.post(`/pqr/cambiar-estado/${id}`).then(res => {
            Swal.fire(
                'Cambiar Estado',
                res.data.Msj,
                'success'
                );
            listarPQR(1);
            }, err => {
            Swal.fire(
                'Cambiar Estado',
                'Error al cambiar el estado de la PQR.',
                'error'
                );
            });
        }
        });
}