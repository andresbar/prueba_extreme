$(document).ready(function(){

    listarPQR(1);

});

function listarPQR(page) {
    axios.get(`/api/pqr/${$('#hdUsuario').val()}?page=${page}`).then(res => {
        $('#tbPQR tr').remove();
        $('.pagination li').remove();
        if(res.data.data.length > 0){
            for(pqr of res.data.data){
                $('#tbPQR').append(`<tr>
                                        <td>${pqr.tipo_descripcion}</td>
                                        <td>${pqr.pqr_asunto}</td>
                                        <td>${moment(pqr.created_at).format('DD/MM/YYYY')}</td>
                                        <td>${((moment().isBefore(moment(pqr.fecha_limite).format('YYYY-MM-DD'))) ? moment(pqr.fecha_limite).format('DD/MM/YYYY') : `<span class="badge badge-danger">${moment(pqr.fecha_limite).format('DD/MM/YYYY')}</span>`)}</td>
                                        <td>${pqr.estado_descripcion}</td>
                                        
                                    </tr>`);
            }

            for(link of res.data.links){
                $('.pagination').append(`<li class="page-item ${((link.active) ? 'active' : '')}"><a class="page-link" ${((link.url) ? `onclick="listarPQR(${parseInt(link.url.slice(-1))})` : '' )}">${link.label}</a></li>`);
            }
        }else{
            $('#tbPQR').append(`<tr><td colspan="7" class="text-center">No hay PQR registradas</td></tr>`);
        }
    });
}