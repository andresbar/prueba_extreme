<?php

namespace App\Exports;

use App\Models\PQR;
use Illuminate\Support\Facades\DB;
use illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PQRUsuarioExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    public function headings(): array
    {
        return [
            'Tipo',
            'Asunto',
            'Fecha',
            'Fecha Limite',
            'Estado',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PQR::select('tipos.tipo_descripcion', 'pqr.pqr_asunto', DB::raw('convert(pqr.created_at, date)'), 'pqr.fecha_limite', 'estados.estado_descripcion', )
            ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
            ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
            ->join('users', 'pqr.usuario_id', '=', 'users.id')
            ->where('pqr.usuario_id', Auth::user()->id)
            ->orderBy('pqr.created_at')
            ->get();
    }
}
