<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    
    public function store(UsuarioRequest $request){
        $user = new User();
        $user->nombre = $request->name;
        $user->email = $request->email;
        $user->rol_id = 2;
        $user->password = bcrypt($request->password);
        $user->api_token = Str::random(60);
        $user->save();

        Auth::loginUsingId($user->id);
        
        return response()->json(['data' => $user->toArray()], 200);

    }

    public function logout(Request $request){
        $user = Auth::guard('api')->user();

        if($user){
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'Se ha cerrado la sesion'], 200);
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        // return $request;

        $user = User::where('email', $request->email)->get()->first();
        if(!$user){
            return response()->json(['data' => 'Usuario y/o contraseña incorrecta'],400);
        }

        if(Hash::check($request->password, $user->password)){
            $user->api_token = Str::random(60);
            $user->save();
            Auth::loginUsingId($user->id);
            
            return response()->json(['data' => $user->toArray()],200);
        }

        return response()->json(['data' => 'Usuario y/o contraseña incorrecta'],400);
    }
}
