<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditPQRRequest;
use App\Http\Requests\PQRRequest;
use App\Models\PQR;
use App\Models\Tipo;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use illuminate\Support\Facades\Auth;
use App\Exports\PQRExport;
use App\Exports\PQRUsuarioExport;

class PQRController extends Controller
{

    /**
     * Crear una nueva instancia de controlador.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->rol_id == 1){
            return PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion', 'users.nombre')
                ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
                ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
                ->join('users', 'pqr.usuario_id', '=', 'users.id')
                ->orderBy('pqr.created_at')
                ->get();
        }else{
            return PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion')
                ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
                ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
                ->where('pqr.usuario_id', Auth::user()->id)
                ->orderBy('pqr.created_at')
                ->get();
        }
    }

    /**
     * Registrar una PQR
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PQRRequest $request)
    {
        $pqr = new PQR;
        $pqr->tipo_id = $request->cbTipo;
        $pqr->pqr_asunto = $request->txtAsunto;
        $pqr->usuario_id = $request->cbUsuario;
        $pqr->estado_id = 1;
        $pqr->fecha_limite = Carbon::now()->add(Tipo::find($request->cbTipo)->tipo_dias, 'day');
        $pqr->save();

        Auth::user()->guardarLog($pqr, 'Crear');

        return ['Msj' => 'Se ha registrado la PQR con éxito.'];
    }

    /**
     * Mostrar informacion de una PQR especifica.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return PQR::find($id);
    }

    /**
     * Actualizar una PQR
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPQRRequest $request, $id)
    {
        $pqr = PQR::find($id);
        $pqr->tipo_id = $request->cbTipoEdit;
        $pqr->pqr_asunto = $request->txtAsuntoEdit;
        $pqr->fecha_limite = Carbon::create(date('Y-m-d',strtotime($pqr->created_at)))->add(Tipo::find($request->cbTipoEdit)->tipo_dias, 'day');
        $pqr->save();

        Auth::user()->guardarLog($pqr, 'Editar');

        return ['Msj' => 'Se ha actualizado la PQR con éxito.'];
    }

    /**
     * Eliminar una PQR.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pqr = PQR::find($id);
        $pqr->delete();
        Auth::user()->guardarLog($pqr, 'Eliminar');
        return ['Msj' => 'Se ha eliminado la PQR con éxito.'];
    }

    /**
     * Cambiar el estado de una PQR.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cambiarEstado($id){
        $pqr = PQR::find($id);
        if(!$pqr){
            return ['Msj' => 'No se encuentra la PQR.'];
        }

        if($pqr->estado_id < 3){
            $pqr->estado_id = $pqr->estado_id + 1;
            $pqr->save();
    
            return ['Msj' => 'Se ha cambiado el estado de la PQR con éxito.'];
        }

        return ['Msj' => 'La PQR no se puede actualizar.'];
    }

    /**
     * Exportar PQR a Excel.
     *
     * @return Maatwebsite\Excel\Facades\Excel
     */
    public function exportarExcel() {
        if(Auth::user()->rol_id == 1){
            return Excel::download(new PQRExport, 'Reporte_PQR.xlsx');
        }else{
            return Excel::download(new PQRUsuarioExport, 'Reporte_PQR.xlsx');
        }
    }

    /**
     * Exportar PQR a CSV.
     *
     * @return Maatwebsite\Excel\Facades\Excel
     */
     public function exportarCsv() {
        if(Auth::user()->rol_id == 1){
            return Excel::download(new PQRExport, 'Reporte_PQR.csv');
        }else{
            return Excel::download(new PQRUsuarioExport, 'Reporte_PQR.csv');
        }
    }

    /**
     * Exportar PQR a PDF.
     *
     * @return \PDF
     */
     public function exportarPDF() {
        if(Auth::user()->rol_id == 1){
            $pqr = PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion', 'users.nombre')
                ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
                ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
                ->join('users', 'pqr.usuario_id', '=', 'users.id')
                ->orderBy('pqr.created_at')
                ->get();
            $pdf = \PDF::loadView('pdf.PQR', ['pqr' => $pqr]);
        }else{
            $pqr = PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion')
                ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
                ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
                ->where('pqr.usuario_id', Auth::user()->id)
                ->orderBy('pqr.created_at')
                ->get();
            $pdf = \PDF::loadView('pdf.PQRUsuario', ['pqr' => $pqr]);
        }
        return $pdf->download('Reporte_PQR.pdf');
    }
}
