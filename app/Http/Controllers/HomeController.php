<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Tipo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Crear una nueva instancia de controlador.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Mostrar el home de la aplicacion.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->rol_id == 1){
            $estados = Estado::all();
            $tipos = Tipo::all();
            $usuarios = User::where('rol_id',2)->get();
            return view('home', compact('estados', 'tipos', 'usuarios'));
        }else{
            return view('homeUsuario');
        }
    }
}
