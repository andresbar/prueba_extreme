<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PQRRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cbTipo' => 'required',
            'txtAsunto' => 'required',
            'cbUsuario' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cbTipo.required' => 'Debe seleccionar el tipo.',
            'txtAsunto.required' => 'Debe ingresar el asunto.',
            'cbUsuario.required' => 'Debe seleccionar el usuario.'
        ];
    }
}
