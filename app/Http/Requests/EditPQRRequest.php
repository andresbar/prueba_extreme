<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditPQRRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cbTipoEdit' => 'required',
            'txtAsuntoEdit' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'cbTipoEdit.required' => 'Debe seleccionar el tipo.',
            'txtAsuntoEdit.required' => 'Debe ingresar el asunto.'
        ];
    }
}
