<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePQRTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pqr', function (Blueprint $table) {
            $table->id('pqr_id');
            $table->bigInteger('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('tipo_id')->on('tipos');
            $table->string('pqr_asunto');
            $table->bigInteger('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('users');
            $table->bigInteger('estado_id')->unsigned();
            $table->foreign('estado_id')->references('estado_id')->on('estados');
            $table->date('fecha_limite');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pqr');
    }
}
