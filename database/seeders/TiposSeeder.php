<?php

namespace Database\Seeders;

use App\Models\Tipo;
use Illuminate\Database\Seeder;

class TiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = [
            [
                'descripcion' => 'Petición',
                'dias' => 7
            ],
            [
                'descripcion' => 'Queja',
                'dias' => 3
            ],
            [
                'descripcion' => 'Reclamo',
                'dias' => 2
            ]
        ];

        foreach($tipos as $tipo){
            Tipo::create([
                'tipo_descripcion' => $tipo['descripcion'],
                'tipo_dias' => $tipo['dias']
            ]);
        }
    }
}
