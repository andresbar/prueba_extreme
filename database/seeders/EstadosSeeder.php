<?php

namespace Database\Seeders;

use App\Models\Estado;
use Illuminate\Database\Seeder;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = ['Nuevo', 'En ejecución', 'Cerrado'];

        foreach($estados as $estado){
            Estado::create([
                'estado_descripcion' => $estado
            ]);
        }
    }
}
