<?php

namespace Database\Seeders;

use App\Models\Rol;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = ['Administrador', 'Usuario'];

        foreach($roles as $rol){
            Rol::create([
                'rol_descripcion' => $rol
            ]);
        }

    }
}
