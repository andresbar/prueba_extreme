<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $usuarios = [
            [
                'nombre' => 'Administrador',
                'email' => 'admin@pqr.com',
                'rol' => 1,
            ],
            [
                'nombre' => 'Usuario',
                'email' => 'usuario@pqr.com',
                'rol' => 2,
            ]
        ];

        foreach($usuarios as $usuario){
            User::create([
                'nombre' => $usuario['nombre'],
                'email' => $usuario['email'],
                'rol_id' => $usuario['rol'],
                'password' => bcrypt('1234')
            ]);
        }

    }
}
