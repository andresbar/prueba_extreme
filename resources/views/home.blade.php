@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row mb-3">
      <div class="col-md-6">
        <h5>Listado de PQR</h5>
      </div>
      <div class="col-md-6 text-right">
          <a href="{{ route('exportar-excel') }}"><button class="btn btn-primary">Excel</button></a>
          <a href="{{ route('exportar-csv') }}"><button class="btn btn-primary">CSV</button></a>
          <a href="{{ route('exportar-pdf') }}"><button class="btn btn-primary">PDF</button></a>
          <button class="btn btn-primary" onclick="$('#mdlRegisto').modal();">Nueva</button>
      </div>
    </div>

    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Usuario</th>
            <th scope="col">Tipo</th>
            <th scope="col">Asunto</th>
            <th scope="col">Fecha</th>
            <th scope="col">Fecha Limite</th>
            <th scope="col">Estado</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="tbPQR">
          
        </tbody>
      </table>

      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          
        </ul>
      </nav>

      {{-- Modal Registrar --}}
      <div class="modal fade" tabindex="-1" role="dialog" id="mdlRegisto">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Registrar PQR</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('pqr.store') }}" id="frmRegistro">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tipo</label>
                      <select name="cbTipo" class="form-control">
                        <option value="">Seleccione..</option>
                        @foreach ($tipos as $tipo)
                            <option value="{{ $tipo->tipo_id }}">{{ $tipo->tipo_descripcion }}</option>
                        @endforeach
                      </select>
                      <small class="text-danger" id="cbTipo-error"></small>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Asunto</label>
                      <input type="text" name="txtAsunto" class="form-control">
                      <small class="text-danger" id="txtAsunto-error"></small>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Usuario</label>
                      <select name="cbUsuario" class="form-control">
                        <option value="">Seleccione..</option>
                        @foreach ($usuarios as $usuario)
                            <option value="{{ $usuario->id }}">{{ $usuario->nombre }}</option>
                        @endforeach
                      </select>
                      <small class="text-danger" id="cbUsuario-error"></small>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="RegistrarPQR()">Guardar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>

      {{-- Modal Actualizar --}}
      <div class="modal fade" tabindex="-1" role="dialog" id="mdlEditar">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Actualizar PQR</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{ route('pqr.update', '') }}" id="frmEditar">
                @csrf
                {{ method_field('PUT') }}
                <input type="hidden" name="hdPqr" id="hdPqr">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Tipo</label>
                      <select name="cbTipoEdit" id="cbTipoEdit" class="form-control">
                        <option value="">Seleccione..</option>
                        @foreach ($tipos as $tipo)
                            <option value="{{ $tipo->tipo_id }}">{{ $tipo->tipo_descripcion }}</option>
                        @endforeach
                      </select>
                      <small class="text-danger" id="cbTipoEdit-error"></small>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Asunto</label>
                      <input type="text" name="txtAsuntoEdit" id="txtAsuntoEdit" class="form-control">
                      <small class="text-danger" id="txtAsuntoEdit-error"></small>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="EditarPQR()">Guardar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>


</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/home.js') }}"></script>
@endsection
