@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row mb-3">
      <div class="col-md-6">
        <h5>Listado de PQR - {{ Auth::user()->nombre }}</h5>
      </div>
      <div class="col-md-6 text-right">
          <a href="{{ route('exportar-excel') }}"><button class="btn btn-primary">Excel</button></a>
          <a href="{{ route('exportar-csv') }}"><button class="btn btn-primary">CSV</button></a>
          <a href="{{ route('exportar-pdf') }}"><button class="btn btn-primary">PDF</button></a>
      </div>
    </div>

    <input type="hidden" id="hdUsuario" value="{{ Auth::user()->id }}">
    <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Tipo</th>
            <th scope="col">Asunto</th>
            <th scope="col">Fecha</th>
            <th scope="col">Fecha Limite</th>
            <th scope="col">Estado</th>
          </tr>
        </thead>
        <tbody id="tbPQR">
          
        </tbody>
      </table>

      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          
        </ul>
      </nav>

      


</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/homeUsuario.js') }}"></script>
@endsection
