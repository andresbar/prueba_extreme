<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- Bootstrap --}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="m-3">
        <h1>Reporte de PQR</h1>
        <hr>
        <table class="table">
            <thead class="thead-dark">
              <tr>
                <th scope="col">Usuario</th>
                <th scope="col">Tipo</th>
                <th scope="col">Asunto</th>
                <th scope="col">Fecha</th>
                <th scope="col">Fecha Limite</th>
                <th scope="col">Estado</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pqr as $row)
                  <tr>
                      <td>{{ $row->nombre }}</td>
                      <td>{{ $row->tipo_descripcion }}</td>
                      <td>{{ $row->pqr_asunto }}</td>
                      <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                      <td>{{ date('d/m/Y', strtotime($row->fecha_limite)) }}</td>
                      <td>{{ $row->estado_descripcion }}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
    </div>
</body>

</html>