<?php

use App\Http\Controllers\PQRController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('/pqr', PQRController::class)->except(['index', 'create', 'show']);
Route::post('/pqr/cambiar-estado/{id}', [PQRController::class, 'cambiarEstado'])->name('cambiar-estado');
Route::get('/pqr/excel', [PQRController::class, 'exportarExcel'])->name('exportar-excel');
Route::get('/pqr/csv', [PQRController::class, 'exportarCsv'])->name('exportar-csv');
Route::get('/pqr/pdf', [PQRController::class, 'exportarPDF'])->name('exportar-pdf');
Route::get('/pqr/view/pdf', function(){
    return view('pdf.PQR');
});
