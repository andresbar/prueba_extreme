<?php

use App\Http\Controllers\api\UsuarioController;
use App\Http\Controllers\PQRController;
use App\Models\PQR;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/usuario', function (Request $request) {
    return $request->user();
});

Route::get('/pqr-web', function(){
    return PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion', 'users.nombre')
        ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
        ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
        ->join('users', 'pqr.usuario_id', '=', 'users.id')
        ->orderBy('pqr.created_at')
        ->paginate(10);
});

Route::get('/pqr/{usuario}', function($usuario){
    return PQR::select('pqr.*', 'tipos.tipo_descripcion', 'estados.estado_descripcion')
        ->join('tipos', 'pqr.tipo_id', '=', 'tipos.tipo_id')
        ->join('estados', 'pqr.estado_id', '=', 'estados.estado_id')
        ->where('pqr.usuario_id', $usuario)
        ->orderBy('pqr.created_at')
        ->paginate(10);
});

Route::post('/usuario', [UsuarioController::class , 'store']);
Route::post('/login', [UsuarioController::class , 'login']);


Route::group(['middleware' => 'auth:api'], function() {
    Route::post('/logout', [UsuarioController::class, 'logout']);
    Route::post('/pqr', [PQRController::class, 'store']);
    Route::get('/pqr', [PQRController::class, 'index']);
    Route::get('/pqr/{id}/edit', [PQRController::class, 'edit']);
    Route::post('/pqr/{id}', [PQRController::class, 'update']);
    Route::delete('/pqr/{id}', [PQRController::class, 'destroy']);
    Route::post('/pqr/cambiar-estado/{id}', [PQRController::class, 'cambiarEstado']);
});
