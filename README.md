# Prueba Desarrollador - Extreme Technologies
Repositorio Git
`git clone https://andresbar@bitbucket.org/andresbar/prueba_extreme.git`

Prueba realizada en laravel y MySQL
### Pasos para ejecutar el proyecto
1. Asegurarse de tener composer instalado
2. Ubicar el directorio del proyecto
3. Instalar los paquetes con `composer install`
4. Crear el arcvhivo .env para configurar la conexion a la base de datos, copiando el contenido del archivo .env.example
5. Configurar la conexion a la base de datos en el archivo .env  
    `DB_CONNECTION=mysql`  
    `DB_HOST=127.0.0.1`  
    `DB_PORT=3306`  
    `DB_DATABASE=laravel`  
    `DB_USERNAME=root`  
    `DB_PASSWORD=`  
6. Configurar API Key con `php artisan key:generate`
7. Una vez configurada la conexion y creada la base de datos, se debe ejecutar las migraciones y seeders para crear la estructura de la base de datos y los datos iniciales para el correcto uso de la aplicacion. Para esto utilizar `php artisan migrate --seed`

Despues de esto tenemos 2 usuarios creados:
###### Rol Administrador
email: admin@pqr.com  
contraseña: 1234

###### Rol Usuario
email: usuario@pqr.com  
contraseña: 1234